from django.contrib import admin
from django.urls import path, include, re_path


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('measurements.urls')),
    path('api/', include('stations.urls')),
    path('api/', include('reports.urls'))
]


urlpatterns += [re_path(r'^silk/', include('silk.urls', namespace='silk'))]
