from stations.models import Station
from rest_framework import serializers

class CustomizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Station
        fields = "__all__"

    def to_representation(self, instance):
        return instance


