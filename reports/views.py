from stations.serializers import StationSerializer
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin

from .mixins import MinMaxMixin, PrecipitationMixin, ReportStationMixin

from stations.models import Station

from .paginations import CustomLimitOffsetPagination
from .serializers import CustomizeSerializer


class ReportMinMaxView(GenericViewSet, MinMaxMixin):
    pagination_class = CustomLimitOffsetPagination
    queryset = Station.objects.all()
    serializer_class = CustomizeSerializer

    def processing_serializer(self, data):
        page = self.paginate_queryset(data)

        serializer = self.get_serializer(data=page, many=True)
        serializer.is_valid()

        return serializer


class ReportPrecipitationView(GenericViewSet, PrecipitationMixin):
    pagination_class = CustomLimitOffsetPagination
    queryset = Station.objects.all()
    serializer_class = CustomizeSerializer


class ReportStationView(GenericViewSet, ReportStationMixin, ListModelMixin):
    pagination_class = CustomLimitOffsetPagination
    queryset = Station.objects.all()
    serializer_class = StationSerializer

    def get_queryset(self, *args):
        query_set = super().get_queryset()

        params = self.request.query_params
        print(params)
        return query_set
