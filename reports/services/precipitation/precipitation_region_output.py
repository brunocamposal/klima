from stations.models import Region
from datetime import date

DATA_ATUAL = date.today()


def precipitation_region_output(year, month, day):
    queryset = Region.objects.prefetch_related(
        "region_set",
    ).all()

    query_data = list()
    data_atual = str(DATA_ATUAL).split("-")

    for region in queryset:
        station_data = region.region_set.all()
        name = region.name
        total_accumulation = 0.0

        for station in station_data:
            rainfall_data = station.rainfallmeasurement.all()

            for rainfall in rainfall_data:
                if rainfall.accumulation_in_hour != None:
                    date = rainfall.moment.date
                    date = str(date).split("-")

                    # filtro por ano, mês e dia
                    if year and month and day:
                        if date[0] == year and int(date[1]) == int(month) and int(date[2]) == int(day):
                            total_accumulation += rainfall.accumulation_in_hour

                    # filtro por ano e mês
                    if year and month and not day:
                        if date[0] == year and int(date[1]) == int(month):
                            total_accumulation += rainfall.accumulation_in_hour

                    # filtro por ano
                    if year and not month and not day:
                        if year and date[0] == year:
                            total_accumulation += rainfall.accumulation_in_hour

                    # sem parametros
                    # comparando com a data atual
                    if not year and not month and not day:
                        if data_atual[0] == year and int(data_atual[1]) == int(month) and int(data_atual[2]) == int(day):
                            total_accumulation += rainfall.accumulation_in_hour

        precipitation_dict = {
            name: {
                "accumulation_in_hour__sum": {
                    "value": total_accumulation
                }
            }
        }

        query_data.append(precipitation_dict)

    return query_data
