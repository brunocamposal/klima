from measurements.models import RadiationMeasurement


def station_radiation_min_and_max(station):
    station_data = station.radiationmeasurement.all()

    radiation_global_value = [
        humidity.global_value
        for humidity in station_data
        if humidity.global_value != None
    ]

    if radiation_global_value:
        maximum = max(radiation_global_value, key=float)
        minimum = min(radiation_global_value, key=float)

        max_date_time = RadiationMeasurement.objects.filter(
            global_value=maximum).first().moment
        min_date_time = RadiationMeasurement.objects.filter(
            global_value=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
