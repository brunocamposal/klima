from measurements.models import RadiationMeasurement


def state_and_region_radiation_min_and_max(query_data: list):
    radiation_global_value = list()

    for station in query_data:
        station_data = station.radiationmeasurement.all()

        for radiation in station_data:
            if radiation.global_value != None:
                radiation_global_value.append(radiation.global_value)

    if radiation_global_value:
        maximum = max(radiation_global_value, key=float)
        minimum = min(radiation_global_value, key=float)

        max_date_time = RadiationMeasurement.objects.filter(
            global_value=maximum).first().moment
        min_date_time = RadiationMeasurement.objects.filter(
            global_value=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
