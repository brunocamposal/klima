from measurements.models import RainfallMeasurement


def state_and_region_rainfall_min_and_max(query_list: list):
    rainfall_accumulation_in_hour = list()

    for station in query_list:
        station_data = station.rainfallmeasurement.all()

        for rainfall in station_data:
            if rainfall.accumulation_in_hour != None:
                rainfall_accumulation_in_hour.append(rainfall.accumulation_in_hour)

    if rainfall_accumulation_in_hour:
        maximum = max(rainfall_accumulation_in_hour, key=float)
        minimum = min(rainfall_accumulation_in_hour, key=float)

        max_date_time = RainfallMeasurement.objects.filter(
            accumulation_in_hour=maximum).first().moment
        min_date_time = RainfallMeasurement.objects.filter(
            accumulation_in_hour=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
