from measurements.models import RainfallMeasurement


def station_rainfall_min_and_max(station):
    station_data = station.rainfallmeasurement.all()
  
    rainfall_accumulation_in_hour = [
        rainfall.accumulation_in_hour
        for rainfall in station_data
        if rainfall.accumulation_in_hour != None
    ]

    if rainfall_accumulation_in_hour:
        maximum = max(rainfall_accumulation_in_hour, key=float)
        minimum = min(rainfall_accumulation_in_hour, key=float)

        max_date_time = RainfallMeasurement.objects.filter(
            accumulation_in_hour=maximum).first().moment
        min_date_time = RainfallMeasurement.objects.filter(
            accumulation_in_hour=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
