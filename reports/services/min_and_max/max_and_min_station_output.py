from stations.models import Station
from .humidity import station_humidity_min_and_max
from .pressure import station_pressure_min_and_max
from .rainfall import station_rainfall_min_and_max
from .temperature import station_temperature_min_and_max
from .wind import station_wind_min_and_max
from .radiation import station_radiation_min_and_max


def max_and_min_station_output():
    queryset = Station.objects.prefetch_related(
        "humiditymeasurement",
        "pressuremeasurement",
        "radiationmeasurement",
        "rainfallmeasurement",
        "temperaturemeasurement",
        "windmeasurement"
    ).all()

    query_data = list()

    for station in queryset:
        name = station.name

        humidity_dict = station_humidity_min_and_max(station)
        pressure_dict = station_pressure_min_and_max(station)
        radiation_dict = station_radiation_min_and_max(station)
        rainfall_dict = station_rainfall_min_and_max(station)
        temperature_dict = station_temperature_min_and_max(station)
        wind_dict = station_wind_min_and_max(station)

        station_dict = {
            name: {
                "pressure": pressure_dict,
                "radiation": radiation_dict,
                "rainfall": rainfall_dict,
                "temperature": temperature_dict,
                "wind": wind_dict,
                "humidity": humidity_dict
            }
        }

        query_data.append(station_dict)

    return query_data
