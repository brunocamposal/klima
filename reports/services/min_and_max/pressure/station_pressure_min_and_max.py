from measurements.models import PressureMeasurement


def station_pressure_min_and_max(station):
    station_data = station.pressuremeasurement.all()

    max_pressure = [
        pressure.maximum
        for pressure in station_data
        if pressure.maximum != None
    ]

    min_pressure = [
        pressure.minimum
        for pressure in station_data
        if pressure.minimum!= None
    ]

    if max_pressure and min_pressure:
        maximum = max(max_pressure, key=float)
        minimum = min(min_pressure, key=float)

        max_date_time = PressureMeasurement.objects.filter(
            maximum=maximum).first().moment
        min_date_time = PressureMeasurement.objects.filter(
            minimum=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
