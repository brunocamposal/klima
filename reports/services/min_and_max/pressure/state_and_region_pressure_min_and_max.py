from measurements.models import PressureMeasurement


def state_and_region_pressure_min_and_max(query_data: list):
    max_pressure = list()
    min_pressure = list()

    for station in query_data:
        station_data = station.pressuremeasurement.all()

        for pressure in station_data:
            if pressure.maximum != None:
                max_pressure.append(pressure.maximum)

            if pressure.minimum != None:
                min_pressure.append(pressure.minimum)

    if max_pressure and min_pressure:
        maximum = max(max_pressure, key=float)
        minimum = min(min_pressure, key=float)

        max_date_time = PressureMeasurement.objects.filter(
            maximum=maximum).first().moment
        min_date_time = PressureMeasurement.objects.filter(
            minimum=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
