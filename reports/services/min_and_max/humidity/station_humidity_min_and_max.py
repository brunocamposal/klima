from measurements.models import HumidityMeasurement


def station_humidity_min_and_max(station):
    station_data = station.humiditymeasurement.all()

    max_humidity = [
        humidity.maximum_relative
        for humidity in station_data
        if humidity.maximum_relative != None
    ]

    min_humidity = [
        humidity.minimum_relative
        for humidity in station_data
        if humidity.minimum_relative != None
    ]

    if max_humidity and min_humidity:
        maximum = max(max_humidity, key=float)
        minimum = min(min_humidity, key=float)

        max_date_time = HumidityMeasurement.objects.filter(
            maximum_relative=maximum).first().moment
        min_date_time = HumidityMeasurement.objects.filter(
            minimum_relative=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
