from measurements.models import HumidityMeasurement


def state_and_region_humidity_min_and_max(query_data: list):
    max_humidity = list()
    min_humidity = list()

    for station in query_data:
        station_data = station.humiditymeasurement.all()

        for humidity in station_data:
            if humidity.maximum_relative != None:
                max_humidity.append(humidity.maximum_relative)

            if humidity.minimum_relative != None:
                min_humidity.append(humidity.minimum_relative)

    if max_humidity and min_humidity:
        maximum = max(max_humidity, key=float)
        minimum = min(min_humidity, key=float)

        max_date_time = HumidityMeasurement.objects.filter(
            maximum_relative=maximum).first().moment
        min_date_time = HumidityMeasurement.objects.filter(
            minimum_relative=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
