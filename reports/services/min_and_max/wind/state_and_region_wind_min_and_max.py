from measurements.models import WindMeasurement


def state_and_region_wind_min_and_max(query_list: list):
    wind_speed = list()

    for station in query_list:
        station_data = station.windmeasurement.all()
        
        for wind in station_data:
            if wind.speed != None:
                wind_speed.append(wind.speed)


    if wind_speed:
        maximum = max(wind_speed, key=float)
        minimum = min(wind_speed, key=float)

        max_date_time = WindMeasurement.objects.filter(
            speed=maximum).first().moment
        min_date_time = WindMeasurement.objects.filter(
            speed=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
