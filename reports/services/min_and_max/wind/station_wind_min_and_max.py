from measurements.models import WindMeasurement


def station_wind_min_and_max(station):
    station_data = station.windmeasurement.all()

    wind_speed = [
        humidity.speed
        for humidity in station_data
        if humidity.speed != None
    ]

    if wind_speed:
        maximum = max(wind_speed, key=float)
        minimum = min(wind_speed, key=float)

        max_date_time = WindMeasurement.objects.filter(
            speed=maximum).first().moment
        min_date_time = WindMeasurement.objects.filter(
            speed=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
