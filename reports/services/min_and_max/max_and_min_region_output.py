from .pressure import state_and_region_pressure_min_and_max
from .rainfall import state_and_region_rainfall_min_and_max
from .temperature import state_and_region_temperature_min_and_max
from .humidity import state_and_region_humidity_min_and_max
from .wind import state_and_region_wind_min_and_max
from .radiation import state_and_region_radiation_min_and_max


from stations.models import Region


def max_and_min_region_output():
    queryset = Region.objects.prefetch_related(
        "region_set",
    ).all()

    query_data = list()

    for region in queryset:
        station_data = region.region_set.all()
        name = region.name

        pressure_dict = state_and_region_pressure_min_and_max(
            station_data)
        radiation_dict = state_and_region_radiation_min_and_max(
            station_data)
        rainfall_dict = state_and_region_rainfall_min_and_max(
            station_data)
        temperature_dict = state_and_region_temperature_min_and_max(
            station_data)
        wind_dict = state_and_region_wind_min_and_max(station_data)
        humidity_dict = state_and_region_humidity_min_and_max(
            station_data)

        region_dict = {
            name: {
                "pressure": pressure_dict,
                "radiation": radiation_dict,
                "rainfall": rainfall_dict,
                "temperature": temperature_dict,
                "wind": wind_dict,
                "humidity": humidity_dict
            }
        }
        
        query_data.append(region_dict)

    return query_data