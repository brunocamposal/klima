from measurements.models import TemperatureMeasurement


def station_temperature_min_and_max(station):
    station_data = station.temperaturemeasurement.all()

    max_temperature = [
        temperature.maximum
        for temperature in station_data
        if temperature.maximum != None
    ]

    min_temperature = [
        temperature.minimum
        for temperature in station_data
        if temperature.minimum!= None
    ]

    if max_temperature and min_temperature:
        maximum = max(max_temperature, key=float)
        minimum = min(min_temperature, key=float)

        max_date_time = TemperatureMeasurement.objects.filter(
            maximum=maximum).first().moment
        min_date_time = TemperatureMeasurement.objects.filter(
            minimum=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
