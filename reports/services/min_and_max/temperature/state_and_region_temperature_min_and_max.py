from measurements.models import TemperatureMeasurement


def state_and_region_temperature_min_and_max(query_list: list):
    max_temperature = list()
    min_temperature = list()

    for station in query_list:
        station_data = station.temperaturemeasurement.all()

        for temperature in station_data:
            if temperature.maximum != None:
                max_temperature.append(temperature.maximum)

            if temperature.minimum != None:
                min_temperature.append(temperature.minimum)

    if max_temperature and min_temperature:
        maximum = max(max_temperature, key=float)
        minimum = min(min_temperature, key=float)

        max_date_time = TemperatureMeasurement.objects.filter(
            maximum=maximum).first().moment
        min_date_time = TemperatureMeasurement.objects.filter(
            minimum=minimum).first().moment

        return {
            "max": {
                "value": maximum,
                "date": f"{max_date_time.date} - {max_date_time.time}"
            },
            "min": {
                "value": minimum,
                "date": f"{min_date_time.date} - {min_date_time.time}"
            },
        }
