from stations.models import State

from .pressure import state_and_region_pressure_min_and_max
from .rainfall import state_and_region_rainfall_min_and_max
from .temperature import state_and_region_temperature_min_and_max
from .humidity import state_and_region_humidity_min_and_max
from .wind import state_and_region_wind_min_and_max
from .radiation import state_and_region_radiation_min_and_max


def max_and_min_state_output():
    queryset = State.objects.prefetch_related(
        "state_set",
    ).all()

    query_data = list()

    for state in queryset:
        station_data = state.state_set.all()
        name = state.name

        pressure_dict = state_and_region_pressure_min_and_max(
            station_data)
        radiation_dict = state_and_region_radiation_min_and_max(
            station_data)
        rainfall_dict = state_and_region_rainfall_min_and_max(
            station_data)
        temperature_dict = state_and_region_temperature_min_and_max(
            station_data)
        wind_dict = state_and_region_wind_min_and_max(station_data)
        humidity_dict = state_and_region_humidity_min_and_max(
            station_data)

        region_dict = {
            name: {
                "pressure": pressure_dict,
                "radiation": radiation_dict,
                "rainfall": rainfall_dict,
                "temperature": temperature_dict,
                "wind": wind_dict,
                "humidity": humidity_dict
            }
        }
        
        query_data.append(region_dict)

    return query_data
