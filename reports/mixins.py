from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action

from .services.min_and_max import max_and_min_station_output, max_and_min_state_output, max_and_min_region_output
from .services.precipitation import precipitation_station_output, precipitation_state_output, precipitation_region_output


class MinMaxMixin:
    @action(detail=False)
    def station(self, *args, **kwargs):
        station_list = max_and_min_station_output()

        serializer = self.processing_serializer(station_list)

        return self.get_paginated_response(serializer.data)

    @action(detail=False)
    def state(self, *args, **kwargs):
        state_list = max_and_min_state_output()

        serializer = self.processing_serializer(state_list)

        return self.get_paginated_response(serializer.data)

    @action(detail=False)
    def region(self, *args, **kwargs):
        region_list = max_and_min_region_output()

        serializer = self.processing_serializer(region_list)

        return self.get_paginated_response(serializer.data)


class PrecipitationMixin:
    @action(detail=False)
    def station(self, *args, **kwargs):
        params = self.request.query_params

        year = params.get("year", "")
        month = params.get("month", "")
        day = params.get("day", "")

        station_list = precipitation_station_output(year, month, day)

        return Response(station_list, status=status.HTTP_200_OK)

    @action(detail=False)
    def state(self, *args, **kwargs):
        params = self.request.query_params

        year = params.get("year", "")
        month = params.get("month", "")
        day = params.get("day", "")

        state_list = precipitation_state_output(year, month, day)

        return Response(state_list, status=status.HTTP_200_OK)

    @action(detail=False)
    def region(self, *args, **kwargs):
        params = self.request.query_params

        year = params.get("year", "")
        month = params.get("month", "")
        day = params.get("day", "")

        region_list = precipitation_region_output(year, month, day)

        return Response(region_list, status=status.HTTP_200_OK)

class ReportStationMixin:

    def lis(self, *args, **kwargs):
        print(args)
        return {}
