from .views import ReportMinMaxView, ReportPrecipitationView, ReportStationView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'min-max', ReportMinMaxView)
router.register(r'precipitation', ReportPrecipitationView)
router.register(r'station-report', ReportStationView)


urlpatterns = router.urls
