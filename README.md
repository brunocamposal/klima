<img src="https://www.eventures.vc/wp-content/uploads/Klima.png" width="200px"/>

# Kklima API

Klima is a RESTful API with climate database. 
## Steps:

- [x] Django project structure and libraries installations
- [x] Create measurements and stations app
- [x] Create models and serializers for respective app
- [x] Configure routes POST, GET, PUT and DELETE from models
- [ ] Create reports app and respective views


developed by <a href="https://gitlab.com/brunocamposal"> <i> brunocamposal </i> </a> :blush:
