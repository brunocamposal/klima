from django.db import models

from stations.models import Station

import datetime


class Moment(models.Model):
    date = models.DateField(default=datetime.date.today)
    time = models.TimeField()


class HumidityMeasurement(models.Model):
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    station = models.ForeignKey(
        Station, related_name="humiditymeasurement", on_delete=models.CASCADE)
    maximum_relative = models.FloatField(null=True)
    minimum_relative = models.FloatField(null=True)
    relative = models.FloatField(null=True)


class TemperatureMeasurement(models.Model):
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    station = models.ForeignKey(
        Station, related_name="temperaturemeasurement", on_delete=models.CASCADE)
    air = models.FloatField(null=True)
    dew_point = models.FloatField(null=True)
    maximum = models.FloatField(null=True)
    minimum = models.FloatField(null=True)
    maximum_dew_point = models.FloatField(null=True)
    minimum_dew_point = models.FloatField(null=True)


class PressureMeasurement(models.Model):
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    station = models.ForeignKey(
        Station, related_name="pressuremeasurement", on_delete=models.CASCADE)
    at_level = models.FloatField(null=True)
    maximum = models.FloatField(null=True)
    minimum = models.FloatField(null=True)


class WindMeasurement(models.Model):
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    station = models.ForeignKey(
        Station, related_name="windmeasurement", on_delete=models.CASCADE)
    direction = models.FloatField(null=True)
    gust = models.FloatField(null=True)
    speed = models.FloatField(null=True)


class RainfallMeasurement(models.Model):
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    station = models.ForeignKey(
        Station, related_name="rainfallmeasurement", on_delete=models.CASCADE)
    accumulation_in_hour = models.FloatField(null=True)


class RadiationMeasurement(models.Model):
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    station = models.ForeignKey(
        Station, related_name="radiationmeasurement", on_delete=models.CASCADE)
    global_value = models.FloatField(null=True)
