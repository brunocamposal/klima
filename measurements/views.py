from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin

from .models import Moment, HumidityMeasurement, TemperatureMeasurement, PressureMeasurement, WindMeasurement, RadiationMeasurement, RainfallMeasurement
from .serializers import MomentSerializer, HumidityMeasurementSerializer, PressureMeasurementSerializer, TemperatureMeasurementSerializer, WindMeasurementSerializer, RadiationMeasurementSerializer, RainfallMeasurementSerializer


class MomentView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = Moment.objects.all()
    serializer_class = MomentSerializer



class HumidityMeasurementView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = HumidityMeasurement.objects.all()
    serializer_class = HumidityMeasurementSerializer



class TemperatureMeasurementView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = TemperatureMeasurement.objects.all()
    serializer_class = TemperatureMeasurementSerializer



class PressureMeasurementView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = PressureMeasurement.objects.all()
    serializer_class = PressureMeasurementSerializer


class WindMeasurementView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = WindMeasurement.objects.all()
    serializer_class = WindMeasurementSerializer



class RadiationMeasurementView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = RadiationMeasurement.objects.all()
    serializer_class = RadiationMeasurementSerializer



class RainfallMeasurementView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = RainfallMeasurement.objects.all()
    serializer_class = RainfallMeasurementSerializer
