from .views import MomentView, HumidityMeasurementView, PressureMeasurementView, TemperatureMeasurementView, RainfallMeasurementView, RadiationMeasurementView, WindMeasurementView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'moments', MomentView)
router.register(r'humidities', HumidityMeasurementView)
router.register(r'pressures', PressureMeasurementView)
router.register(r'rainfalls', RainfallMeasurementView)
router.register(r'radiations', RadiationMeasurementView)
router.register(r'winds', WindMeasurementView)
router.register(r'temperatures', TemperatureMeasurementView)


urlpatterns = router.urls
