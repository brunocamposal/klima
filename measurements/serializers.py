from rest_framework import serializers

from .models import Moment, HumidityMeasurement, RainfallMeasurement, RadiationMeasurement, TemperatureMeasurement, PressureMeasurement, WindMeasurement


class MomentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Moment
        fields = "__all__"


class HumidityMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = HumidityMeasurement
        fields = "__all__"


class RainfallMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = RainfallMeasurement
        fields = "__all__"


class PressureMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = PressureMeasurement
        fields = "__all__"


class WindMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = WindMeasurement
        fields = "__all__"


class RadiationMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = RadiationMeasurement
        fields = "__all__"


class TemperatureMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemperatureMeasurement
        fields = "__all__"
