from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin, RetrieveModelMixin, CreateModelMixin, ListModelMixin

from .models import Station, State, Region
from .serializers import StationSerializer, StateSerializer, RegionSerializer


class StationView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = Station.objects.all()
    serializer_class = StationSerializer


class StateView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = State.objects.all()
    serializer_class = StateSerializer


class RegionView(GenericViewSet, ListModelMixin, CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
