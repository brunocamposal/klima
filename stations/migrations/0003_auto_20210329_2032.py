# Generated by Django 3.1.7 on 2021-03-29 20:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('stations', '0002_auto_20210329_1909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='station',
            name='region',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='region_set', to='stations.region'),
        ),
    ]
