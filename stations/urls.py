from rest_framework.routers import DefaultRouter

from .views import StationView, StateView, RegionView

router = DefaultRouter()
router.register(r'stations', StationView)
router.register(r'states', StateView)
router.register(r'regions', RegionView)

urlpatterns = router.urls
