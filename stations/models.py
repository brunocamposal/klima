from django.db import models


class State(models.Model):
    name = models.CharField(max_length=125)


class Region(models.Model):
    name = models.CharField(max_length=125)


class Station(models.Model):
    name = models.CharField(max_length=125)
    code = models.CharField(max_length=50)
    region = models.ForeignKey(
        Region, related_name="region_set", on_delete=models.CASCADE)
    state = models.ForeignKey(
        State, related_name="state_set", on_delete=models.CASCADE)
    latitude = models.FloatField()
    longitude = models.FloatField()
    altitude = models.FloatField()
